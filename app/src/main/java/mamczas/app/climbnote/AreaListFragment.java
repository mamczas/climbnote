package mamczas.app.climbnote;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import mamczas.app.climbnote.Data.Area;
import mamczas.app.climbnote.Data.AreaDataAccess;
import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-05-05.
 */
public class AreaListFragment extends ListFragment implements OnItemClickListener, AdapterView.OnItemLongClickListener {
    private List<Area> data;
    private Context context;
    private Menu menu;
    private long sectorID;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.area_list, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity().getApplicationContext();
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
        setHasOptionsMenu(true);
        Bundle extras = getArguments();
        if (extras != null) {
            sectorID = extras.getLong(ConstDB.SECTOR_ID);
            loadData(sectorID);
        } else {
            loadData(-1);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Intent empDetailsIntent = new Intent(context, AddAreaActivity.class);
        Area area = data.get(position);
        empDetailsIntent.putExtra(ConstDB.ID, area.getId());
        empDetailsIntent.putExtra(ConstDB.SECTOR_ID, sectorID);
        startActivityForResult(empDetailsIntent, 202);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new RoadListFragment();
        Area area = data.get(position);
        Bundle data = new Bundle();
        data.putLong(ConstDB.AREA_ID, area.getId());
        data.putLong(ConstDB.SECTOR_ID, area.getSector().getId());

        if (fragment != null) {
            fragment.setArguments(data);
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment).addToBackStack(null).commit();
        }
    }

    private void loadData(long id) {
        DataAccess da = AreaDataAccess.create(context);
        if (id == -1) {
            data = da.getAll();
        } else {
            data = da.getListByID(id);
        }
        ArrayAdapter<Area> adapter = new ArrayAdapter<>(context, R.layout.area_item, data);
        setListAdapter(adapter);
    }

    private void setItemOnList(long idItem) {
        setSelection((int) idItem - 1);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem setting = menu.findItem(R.id.action_settings);
        MenuItem addNew = menu.findItem(R.id.action_add_new);
        setting.setVisible(false);
        addNew.setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add_new:
                Intent empDetailsIntent = new Intent(context, AddAreaActivity.class);
                empDetailsIntent.putExtra(ConstDB.SECTOR_ID, sectorID);
                startActivityForResult(empDetailsIntent, 101);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bundle extra = data.getExtras();
            if (extra != null) {
                long idItem = extra.getLong(ConstDB.AREA_ID, -1);
                long idSector = extra.getLong(ConstDB.SECTOR_ID);
                loadData(idSector);
                //getListView().smoothScrollToPosition((int)idItem - 1);
            } else {
                loadData(-1);
            }

        }
    }
}
