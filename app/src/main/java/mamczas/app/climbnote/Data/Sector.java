package mamczas.app.climbnote.Data;

/**
 * Created by michal.mamcarz on 2016-05-17.
 */
public class Sector {
    private long id;
    private String name;
    private GPS position;
    private String Description;

    public Sector(String name, GPS position, String description) {
        this.name = name;
        this.position = position;
        Description = description;
    }

    public Sector() {

    }

    public void setPosition(GPS position) {
        this.position = position;
    }

    public GPS getPosition(){
        return this.position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPositionString() {
        return position.toString();
    }

    public void setPosition(String position) {
        this.position = new GPS(position);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }


}
