package mamczas.app.climbnote.Data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-08-31.
 */
public class AreaDataAccess implements DataAccess<Area> {

    private final String getALL = String.format("SELECT a.%s, a.%s, a.%s, b.%s, b.%s " +
            "FROM %s a INNER JOIN %s b ON a.%s=b.%s",
            ConstDB.ID,ConstDB.NAME, ConstDB.GPS_POSITION,ConstDB.NAME,ConstDB.ID,
            ConstDB.AREA, ConstDB.SECTOR, ConstDB.SECTOR_ID, ConstDB.ID);
    private final String getByID = String.format("SELECT a.%s, a.%s, a.%s, b.%s, b.%s " +
                    "FROM %s a INNER JOIN %s b ON a.%s=b.%s WHERE a.%s=?",
            ConstDB.ID,ConstDB.NAME, ConstDB.GPS_POSITION,ConstDB.NAME,ConstDB.ID,
            ConstDB.AREA, ConstDB.SECTOR, ConstDB.ID, ConstDB.ID, ConstDB.ID);
    private final String getListByID = String.format("SELECT a.%s, a.%s, a.%s, b.%s, b.%s " +
                    "FROM %s a INNER JOIN %s b ON a.%s=b.%s WHERE b.%s=?",
            ConstDB.ID,ConstDB.NAME, ConstDB.GPS_POSITION,ConstDB.NAME,ConstDB.ID,
            ConstDB.AREA, ConstDB.SECTOR, ConstDB.SECTOR_ID, ConstDB.ID, ConstDB.ID);
    private final String getNamesQuery = String.format("SELECT r.%s, r.%s FROM %s r WHERE r.%s=?", ConstDB.ID, ConstDB.NAME, ConstDB.AREA, ConstDB.SECTOR_ID);

    private final SQLiteOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private static AreaDataAccess instance = null;


    private AreaDataAccess(Context ctx) {
        dbOpenHelper = new OpenDBHelper(ctx);
        database = dbOpenHelper.getWritableDatabase();
    }

    public static DataAccess create(Context ctx) {
        if (instance == null) {
            instance = new AreaDataAccess(ctx);
        }
        return instance;
    }

    @Override
    public List<Area> getAll() {
        List<Area> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getALL, null)) {

            while (c.moveToNext()) {
                Area e = new Area(c.getLong(0),c.getString(1),new GPS(c.getString(2)),new Sector());
                e.getSector().setName(c.getString(3));
                e.getSector().setId(c.getInt(4));
                result.add(e);
            }
        }
        return result;
    }

    @Override
    public long insert(Area e) {
        ContentValues cv = new ContentValues();
        cv.put(ConstDB.NAME, e.getName());
        cv.put(ConstDB.SECTOR_ID, e.getSector().getId());
        cv.put(ConstDB.GPS_POSITION, e.getPositionString());
        long id = database.insert(ConstDB.AREA, null, cv);
        e.setId(id);
        return id;
    }

    @Override
    public void update(Area e) {
        ContentValues cv = new ContentValues();
        cv.put(ConstDB.NAME, e.getName());
        cv.put(ConstDB.SECTOR_ID, e.getSector().getId());
        cv.put(ConstDB.GPS_POSITION, e.getPositionString());
        database.update(ConstDB.AREA, cv, ConstDB._ID + e.getId(), null);
    }

    @Override
    public void delete(Area e) {
        delete(e.getId());
    }

    @Override
    public void delete(long id) {
        database.delete(ConstDB.AREA, ConstDB._ID + id, null);
    }

    @Override
    public Area getById(long id) {
        try (Cursor c = database.rawQuery(getByID, new String[]{String.valueOf(id)})) {

            if (c.moveToNext()) {
                Area e = new Area(c.getLong(0),c.getString(1),new GPS(c.getString(2)),new Sector());
                e.getSector().setName(c.getString(3));
                e.getSector().setId(c.getInt(4));
                return e;
            } else {
                return null;
            }
        }
    }



    @Override
    public Cursor getCursor() {
        try (Cursor c = database.query(ConstDB.AREA, null /* all */,
                null, null, null, null, ConstDB.NAME, null)) {
            if (c != null) {
                return c;
            }
            return null;
        }
    }

    @Override
    public long put(Area e) {
        long id = isInDB(e.getName());
        if(id == -1){
            return insert(e);
        }
        else {
            e.setId(id);
            update(e);
            return id;
        }
    }

    @Override
    public List<Area> getListByID(long id) {
        List<Area> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getListByID, new String[]{String.valueOf(id)})) {
            while (c.moveToNext()) {
                Area e = new Area(c.getLong(0),c.getString(1),new GPS(c.getString(2)),new Sector());
                e.getSector().setName(c.getString(3));
                e.getSector().setId(c.getInt(4));
                result.add(e);
            }
        }
        return result;
    }

    @Override
    public List<Area> getNames(long id) {
        List<Area> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getNamesQuery, new String[]{String.valueOf(id)})) {
            while (c.moveToNext()) {
                Area e = new Area(c.getLong(0));
                e.setName(c.getString(1));
                result.add(e);
            }
        }
        return result;
    }

    @Override
    public long isInDB(String name) {
        try (Cursor c = database.query(ConstDB.AREA, null /* all */,
                ConstDB.NAME+"="+"'"+name.trim()+"'", null, null, null, ConstDB.NAME, null)) {
            if (c.moveToFirst()) {
                return c.getLong(0);
            } else {
                return -1;
            }
        }
    }

}
