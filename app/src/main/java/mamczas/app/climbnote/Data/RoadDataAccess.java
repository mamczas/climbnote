package mamczas.app.climbnote.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-09-08.
 */
public class RoadDataAccess implements DataAccess<Road> {

    private static RoadDataAccess instance = null;
    private final String getALL = String.format("SELECT r.%s, r.%s, r.%s, r.%s, r.%s, r.%s, r.%s " +
                    "FROM %s as r",
            ConstDB.ID, ConstDB.NAME, ConstDB.GRADE, ConstDB.STYLE, ConstDB.SECTOR_ID, ConstDB.AREA_ID, ConstDB.TIME, ConstDB.ROAD);
    private final String getListByID = String.format("SELECT r.%s, r.%s, r.%s, r.%s, r.%s, r.%s, r.%s " +
                    "FROM %s as r WHERE r.%s=?",
            ConstDB.ID, ConstDB.NAME, ConstDB.GRADE, ConstDB.STYLE, ConstDB.SECTOR_ID, ConstDB.AREA_ID, ConstDB.TIME, ConstDB.ROAD, ConstDB.AREA_ID);
    private final String getByID = String.format("SELECT r.%s, r.%s, r.%s, r.%s, r.%s, r.%s, r.%s " +
                    "FROM %s as r WHERE r.%s=?",
            ConstDB.ID, ConstDB.NAME, ConstDB.GRADE, ConstDB.STYLE, ConstDB.SECTOR_ID, ConstDB.AREA_ID, ConstDB.TIME, ConstDB.ROAD, ConstDB.ID);
    private final String getTOP10 = String.format("SELECT r.%s, r.%s, r.%s, r.%s " +
                    "FROM %s as r WHERE r.%s>=? ORDER BY %s DESC LIMIT 10",
            ConstDB.ID, ConstDB.NAME, ConstDB.GRADE, ConstDB.STYLE, ConstDB.ROAD, ConstDB.TIME, ConstDB.TIME);

    private final SQLiteOpenHelper dbOpenHelper;
    private SQLiteDatabase database;

    private RoadDataAccess(Context ctx) {
        dbOpenHelper = new OpenDBHelper(ctx);
        database = dbOpenHelper.getWritableDatabase();
    }

    public static DataAccess create(Context ctx) {
        if (instance == null) {
            instance = new RoadDataAccess(ctx);
        }
        return instance;
    }

    @Override
    public List<Road> getAll() {
        List<Road> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getALL, null)) {
            while (c.moveToNext()) {
                Road e = new Road(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getInt(4), c.getInt(5), c.getInt(6));
                result.add(e);
            }
        }
        return result;
    }

    //.NAME,.SECTOR_ID,.AREA_ID,.GRADLE,.STYLE,Time
    @Override
    public long insert(Road road) {
        ContentValues cv = new ContentValues();
        cv.put(ConstDB.NAME, road.getName());
        cv.put(ConstDB.SECTOR_ID, road.getSectorID());
        cv.put(ConstDB.AREA_ID, road.getAreaID());
        cv.put(ConstDB.GRADE, road.getGrade());
        cv.put(ConstDB.STYLE, road.getStyle());
        cv.put(ConstDB.TIME, road.getTime());
        long id = database.insert(ConstDB.ROAD, null, cv);
        road.setId(id);
        return id;
    }

    @Override
    public void update(Road road) {
        ContentValues cv = new ContentValues();
        cv.put(ConstDB.NAME, road.getName());
        cv.put(ConstDB.SECTOR_ID, road.getSectorID());
        cv.put(ConstDB.AREA_ID, road.getAreaID());
        cv.put(ConstDB.GRADE, road.getGrade());
        cv.put(ConstDB.STYLE, road.getStyle());
        cv.put(ConstDB.TIME, road.getTime());
        database.update(ConstDB.ROAD, cv, ConstDB._ID + road.getId(), null);
    }

    @Override
    public void delete(Road e) {
        delete(e.getId());
    }

    @Override
    public void delete(long id) {
        database.delete(ConstDB.ROAD, ConstDB._ID + id, null);
    }

    @Override
    public Road getById(long id) {
        try (Cursor c = database.rawQuery(getByID, new String[]{String.valueOf(id)})) {
            if (c.moveToFirst()) {
                Road e = new Road(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getInt(4), c.getInt(5), c.getInt(6));
                return e;
            } else {
                return new Road();
            }
        }
    }

    @Override
    public Cursor getCursor() {
        return null;
    }

    @Override
    public long isInDB(String name) {
        try (Cursor c = database.query(ConstDB.ROAD, null /* all */,
                ConstDB.NAME + "=" + "'" + name.trim() + "'", null, null, null, ConstDB.NAME, null)) {
            if (c.moveToFirst()) {
                return c.getLong(0);
            } else {
                return -1;
            }
        }
    }

    @Override
    public long put(Road e) {
        long id = isInDB(e.getName());
        if (id == -1) {
            return insert(e);
        } else {
            e.setId(id);
            update(e);
            return id;
        }
    }

    @Override
    public List<Road> getListByID(long id) {
        List<Road> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getListByID, new String[]{String.valueOf(id)})) {
            while (c.moveToNext()) {
                Road e = new Road(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getInt(4), c.getInt(5), c.getInt(6));
                result.add(e);
            }
        }
        return result;
    }

    @Override
    public List<Road> getNames(long id) {
        List<Road> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getTOP10, new String[]{String.valueOf(id)})) {
            while (c.moveToNext()) {
                String name = c.getString(1);
                if (name != null) {
                    Road e = new Road(c.getInt(0), name, c.getInt(2), c.getInt(3));
                    result.add(e);
                }
            }
        }
        return result;
    }
}
