package mamczas.app.climbnote.Data;

/**
 * Created by michal.mamcarz on 2016-05-17.
 */
public class GPS {

    private Double longitude;
    private Double latitude;

    public GPS(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public GPS(String position) {
        String[] tmp = position.split(";");
        if (tmp.length == 2) {
            longitude = Double.parseDouble(tmp[1]);
            latitude = Double.parseDouble(tmp[0]);
        } else {
            longitude = 0.0;
            latitude = 0.0;
        }
    }

    public static String ConvertToGPS(String value) {
        String[] tmp = value.split(" ");
        if (tmp.length == 8) {
            StringBuilder out = new StringBuilder();
            for (int i = 0; i < 8; i++) {
                switch (i) {
                    case 1:
                        out.append(tmp[i] + '°');
                        break;
                    case 2:
                        out.append((tmp[i]) + '`');
                        break;
                    case 3:
                        out.append(tmp[i] + "``;");
                        break;
                    case 5:
                        out.append(tmp[i] + '°');
                        break;
                    case 6:
                        out.append((tmp[i]) + '`');
                        break;
                    case 7:
                        out.append(tmp[i] + "``");
                        break;
                    default:
                        out.append(tmp[i] + " ");
                }
            }
            return out.toString();
        } else {
            return "";
        }
    }

    public static String ConvertToDisplay(String value) {
        if (value != "") try {
            String tmp = value.substring(2);
            tmp = tmp.replace("°", " ");
            tmp = tmp.replace("`", " ");
            return tmp.replace("``", "");
        } catch (Exception e) {
            return "";
        }
        else return value;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return latitude.toString() + ';' + longitude.toString();
    }

    public char getlatitudeNEWS() {
        if (latitude > 0) {
            return 0;
        } else return 1;
    }

    public int getLongitudeST() {
        return Math.abs(longitude.intValue());
    }

    public int getLatitudeST() {
        return Math.abs(latitude.intValue());
    }

    public int getLongitudeMin() {
        Double tmp = (longitude % 1) * 60;
        return Math.abs(tmp.intValue());
    }

    public int getLatitudeMin() {
        Double tmp = (latitude % 1) * 60;
        return Math.abs(tmp.intValue());
    }

    public Double getLatitudeSEC() {
        Double tmp = (latitude % 1) * 60;
        return Math.abs((tmp % 1) * 60);
    }

    public Double getLongitudeSEC() {
        Double tmp = (longitude % 1) * 60;
        return Math.abs((tmp % 1) * 60);
    }


    public int getlongitudeNEWS() {
        if (longitude > 0) {
            return 0;
        } else return 1;
    }

}
