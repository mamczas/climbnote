package mamczas.app.climbnote.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-05-17.
 */
public class SectorDataAccess implements DataAccess<Sector> {
    private final SQLiteOpenHelper dbOpenHelper;
    private SQLiteDatabase database;
    private static SectorDataAccess instance = null;
    private static String getNamesQuery = String.format("SELECT r.%s, r.%s FROM %s r", ConstDB.ID, ConstDB.NAME, ConstDB.SECTOR);

    private SectorDataAccess(Context ctx) {
        dbOpenHelper = new OpenDBHelper(ctx);
        database = dbOpenHelper.getWritableDatabase();
    }

    public static DataAccess create(Context ctx) {
        if (instance == null) {
            instance = new SectorDataAccess(ctx);
        }
        return instance;
    }

    @Override
    public List<Sector> getAll() {
        List<Sector> result = new ArrayList<>();
        try (Cursor c = database.query(ConstDB.SECTOR, null /* all */,
                null, null, null, null, ConstDB.ID, null)) {

            while (c.moveToNext()) {
                Sector e = new Sector();
                e.setId(c.getLong(0));
                e.setName(c.getString(1));
                e.setPosition(c.getString(2));
                result.add(e);
            }
        }
        return result;
    }

    @Override
    public long insert(Sector e) {
        ContentValues cv = new ContentValues();
        cv.put(ConstDB.NAME, e.getName());
        cv.put(ConstDB.GPS_POSITION, e.getPositionString());
        long id = database.insert(ConstDB.SECTOR, null, cv);
        e.setId(id);
        return id;
    }

    @Override
    public void update(Sector e) {
        ContentValues cv = new ContentValues();
        cv.put(ConstDB.NAME, e.getName());
        cv.put(ConstDB.GPS_POSITION, e.getPositionString());

        database.update(ConstDB.SECTOR, cv, ConstDB._ID + e.getId(), null);
    }

    @Override
    public void delete(Sector e) {
        delete(e.getId());
    }

    @Override
    public void delete(long id) {
        database.delete(ConstDB.SECTOR, ConstDB._ID + id, null);
    }

    @Override
    public Sector getById(long id) {
        try (Cursor c = database.query(ConstDB.SECTOR, null /* all */,
                ConstDB._ID + id, null, null, null, ConstDB.NAME, null)) {
            if (c.moveToFirst()) {
                Sector e = new Sector();
                e.setId(c.getLong(0));
                e.setName(c.getString(1));
                e.setPosition(c.getString(2));
                return e;
            } else {
                return null;
            }
        }
    }

    public long isInDB(String name) {
        try (Cursor c = database.query(ConstDB.SECTOR, null /* all */,
                ConstDB.NAME+"="+"'"+name.trim()+"'", null, null, null, ConstDB.NAME, null)) {
            if (c.moveToFirst()) {
                return c.getLong(0);
            } else {
                return -1;
            }
        }
    }

    @Override
    public Cursor getCursor() {
        try (Cursor c = database.query(ConstDB.SECTOR, null /* all */,
                null, null, null, null, ConstDB.NAME, null)) {
            if (c != null) {
                return c;
            }
            return null;
        }
    }

    @Override
    public long put(Sector e) {
        long id = isInDB(e.getName());
        if(id == -1){
           return insert(e);
        }
        else {
            e.setId(id);
            update(e);
            return id;
        }
    }

    @Override
    public List<Sector> getListByID(long id) {
        return null;
    }

    @Override
    public List<Sector> getNames(long id) {
        List<Sector> result = new ArrayList<>();
        try (Cursor c = database.rawQuery(getNamesQuery, null)) {
            while (c.moveToNext()) {
                Sector e = new Sector();
                e.setId(c.getLong(0));
                e.setName(c.getString(1));
                result.add(e);
            }
        }
        return result;
    }
}