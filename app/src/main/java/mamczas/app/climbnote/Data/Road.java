package mamczas.app.climbnote.Data;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-09-07.
 */
public class Road {


    private long id;
    private String name;
    private int grade;
    private int style;
    private long sectorID;
    private Calendar date;
    private Area area;

    public Road(long id, String name, int grade, int style, long sectorID, long areaID, int date) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.style = style;
        this.sectorID = sectorID;
        this.area = new Area(areaID);
        this.date = ConstDB.stringToDate(date);
    }

    public Road() {
        this.sectorID = -1;
        this.area = new Area(-1);
        this.grade = 7;
        this.style = 1;
        this.date = Calendar.getInstance(Locale.ROOT);
    }

    public Road(long id, String name, int grade, int style) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.style = style;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public int getTime() {
        return ConstDB.dateToString(date);
    }

    public long getSectorID() {
        return sectorID;
    }

    public void setSectorID(long sectorID) {
        this.sectorID = sectorID;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public long getAreaID() {
        return getArea().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    private String getGradeString(){
        return ConstDB.grades.get(this.grade).getDesc();
    }

    private String getStyleString(){
        return ConstDB.styles.get(this.style).getDesc();
    }

    @Override
    public String toString() {
        return getName() + ": " + getGradeString() + "(" + getStyleString() + ")";
    }

}
