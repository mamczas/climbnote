package mamczas.app.climbnote.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-05-17.
 */
public class OpenDBHelper extends SQLiteOpenHelper {

    public OpenDBHelper(Context ctx) {super(ctx, ConstDB.DB_NAME, null, ConstDB.DB_VERSION);}

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = String.format("CREATE TABLE %s (_id INTEGER PRIMARY KEY, "
                + "%s TEXT NOT NULL, "
                + "%s TEXT);",ConstDB.SECTOR,
                    ConstDB.NAME, ConstDB.GPS_POSITION);
        db.execSQL(sql);
        //creating area table
        sql = String.format("CREATE TABLE %s (_id INTEGER PRIMARY KEY, "
                + "%s TEXT NOT NULL,"
                + "%s INTEGER,"
                + "%s TEXT,"
                + "FOREIGN KEY(%s) REFERENCES %s(%s) ON DELETE CASCADE);",ConstDB.AREA,
                    ConstDB.NAME,ConstDB.SECTOR_ID, ConstDB.GPS_POSITION,
                    ConstDB.SECTOR_ID, ConstDB.SECTOR, ConstDB.ID
                );
        db.execSQL(sql);

        sql = String.format("CREATE TABLE %s (_id INTEGER PRIMARY KEY, "
                + "%s TEXT NOT NULL, "
                + "%s INTEGER, "
                + "%s INTEGER, "
                + "%s INTEGER, "
                + "%s INTEGER, "
                + "%s INTEGER, "
                + "FOREIGN KEY(%s) REFERENCES %s(%s) ON DELETE CASCADE "
                + "FOREIGN KEY(%s) REFERENCES %s(%s) ON DELETE CASCADE);"
                ,ConstDB.ROAD,
                    ConstDB.NAME,ConstDB.SECTOR_ID,ConstDB.AREA_ID,ConstDB.GRADE,ConstDB.STYLE,ConstDB.TIME,
                    ConstDB.SECTOR_ID, ConstDB.SECTOR, ConstDB.ID,
                    ConstDB.AREA_ID, ConstDB.AREA, ConstDB.ID);
        db.execSQL(sql);

        sql = String.format("INSERT INTO %s VALUES (null, 'Dolina Kobylańska', '50.294998,19.644577')",ConstDB.SECTOR);
        db.execSQL(sql);
        sql = String.format("INSERT INTO %s VALUES (null, 'Dolina Bedkowska', '2.165164;-10.524902')",ConstDB.SECTOR);
        db.execSQL(sql);
        sql = String .format("INSERT INTO %s VALUES (null, 'Żabi Koń',1, '-2.165164;10.524902')",ConstDB.AREA);
        db.execSQL(sql);
        sql = String .format("INSERT INTO %s VALUES (null, 'Cmentarzysko',2, '2.165164;10.524902')",ConstDB.AREA);
        db.execSQL(sql);
//        sql = String.format("INSERT INTO %s VALUES (null, 'Piwo dla bezdomnych',1,1,3,8)",ConstDB.ROAD);
//        db.execSQL(sql);
//        sql = String.format("INSERT INTO %s VALUES (null, 'Żabi Koń',1,1,1,10)",ConstDB.ROAD);
//        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
