package mamczas.app.climbnote.Data;

/**
 * Created by michal.mamcarz on 2016-08-31.
 */
public class Area {
    private long id;
    private String name;
    private GPS position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GPS getPosition() {
        return position;
    }

    public String getPositionString() {
        return position.toString();
    }

    public void setPosition(GPS position) {
        this.position = position;
    }

    public Sector getSector() {
        return sector;
    }


    public long getId() {
        return id;
    }


    private Sector sector;

    public Area(long id){
        this.id = id;
    }

    public Area(long id, String name, GPS position, Sector sector) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.sector = sector;
    }

    public Area( String name, GPS position, Sector sector) {
        this.id =-1;
        this.name = name;
        this.position = position;
        this.sector = sector;
    }

    @Override
    public String toString() {
        return  getName();
    }

    public void setId(long id) {
        this.id = id;
    }
}
