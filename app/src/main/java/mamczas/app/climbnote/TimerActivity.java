package mamczas.app.climbnote;

import android.app.FragmentManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import java.util.ArrayList;
import mamczas.app.climbnote.Fragments.HeadlessTimerFragment;
import mamczas.app.climbnote.Interfaces.TaskStatusCallback;


public class TimerActivity extends AppCompatActivity implements TaskStatusCallback {

    private static final String ARG_parameters = "parameters";
    private static final String ARG_run = "runB";
    private final int repeatN = 5;
    private final int restN = 30;
    private final int hangN = 10;
    private final int pauseN = 30;
    private NumberPicker repeat;
    private NumberPicker rest;
    private NumberPicker hang;
    private NumberPicker pause;
    private Button button;
    private TextView lblrepeat;
    private TextView lblrest;
    private TextView lblhang;
    private TextView lblpause;

    private MediaPlayer mediaPlayer;
    private ArrayList<Integer> parameters = new ArrayList<Integer>();

    private HeadlessTimerFragment mFragment;
    private boolean run = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_timer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.timerToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.onCreate(savedInstanceState);

        rest = (NumberPicker) findViewById(R.id.NP_rest);
        repeat = (NumberPicker) findViewById(R.id.NP_rep);
        hang = (NumberPicker) findViewById(R.id.NP_hang);
        pause = (NumberPicker) findViewById(R.id.NP_pause);

        lblrepeat = (TextView) findViewById(R.id.txt_rep);
        lblrest = (TextView) findViewById(R.id.txt_rest);
        lblhang = (TextView) findViewById(R.id.txt_hang);
        button = (Button) findViewById(R.id.timer_Button);
        mediaPlayer = MediaPlayer.create(this, R.raw.sound_1);

        if (savedInstanceState != null) {
            parameters = savedInstanceState.getIntegerArrayList(ARG_parameters);
            run = savedInstanceState.getBoolean(ARG_run, false);
        } else {
            parameters = new ArrayList<Integer>();
            parameters.add(repeatN);
            parameters.add(hangN);
            parameters.add(restN);
            parameters.add(pauseN);
        }

        FragmentManager mMgr = getFragmentManager();
        mFragment = (HeadlessTimerFragment) mMgr
                .findFragmentByTag(HeadlessTimerFragment.TAG_HEADLESS_FRAGMENT);

        if (mFragment == null) {
            mFragment = new HeadlessTimerFragment();
            mMgr.beginTransaction()
                    .add(mFragment, HeadlessTimerFragment.TAG_HEADLESS_FRAGMENT)
                    .commit();
        }

        prepareNumberPicker();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                run = !run;
                enableNumberPickers(run);
                start(run);
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(ARG_parameters, parameters);
        outState.putBoolean(ARG_run, run);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mFragment != null) {
            mFragment.cancelBackgroundTask();
        }
        mediaPlayer.stop();
        super.onBackPressed();
    }


    private void start(boolean run) {
        if (!run) {
            if (mFragment != null) {
                mFragment.startBackgroundTask(this.parameters);
                button.setText(R.string.stop);
            }

        } else {
            if (mFragment != null) {
                mFragment.cancelBackgroundTask();
                button.setText(R.string.start);
            }
        }
    }


    private void prepareNumberPicker() {
        enableNumberPickers(run);
        rest.setMinValue(1);
        rest.setMaxValue(100);
        rest.setValue(parameters.get(2));
        rest.setWrapSelectorWheel(true);
        rest.setOnValueChangedListener(new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
                parameters.set(2, newVal);
            }
        });
        repeat.setMinValue(1);
        repeat.setMaxValue(100);
        repeat.setValue(parameters.get(0));
        repeat.setWrapSelectorWheel(true);
        repeat.setOnValueChangedListener(new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
                parameters.set(0, newVal);
            }
        });
        hang.setMinValue(1);
        hang.setMaxValue(100);
        hang.setValue(parameters.get(1));
        hang.setWrapSelectorWheel(true);
        hang.setOnValueChangedListener(new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
                parameters.set(1, newVal);
            }
        });
        pause.setMinValue(1);
        pause.setMaxValue(100);
        pause.setValue(parameters.get(3));
        pause.setWrapSelectorWheel(true);
        pause.setOnValueChangedListener(new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
                parameters.set(3, newVal);
            }
        });
    }

    // Background task Callbacks

    @Override
    public void onPreExecute() {}

    @Override
    public void onPostExecute() {
        if (mFragment != null)
            mFragment.updateExecutingStatus(false);
    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onProgressUpdate(Integer... progress) {
        lblrepeat.setText(progress[0].toString());
        lblhang.setText(progress[1].toString());
        lblrest.setText(progress[2].toString());
        if(progress[1] == 0) {
            mediaPlayer.start();
        }
    }

    private void enableNumberPickers(Boolean run) {
        rest.setEnabled(run);
        hang.setEnabled(run);
        pause.setEnabled(run);
        repeat.setEnabled(run);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
