package mamczas.app.climbnote;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

import java.util.Calendar;
import java.util.List;

import mamczas.app.climbnote.Data.Road;
import mamczas.app.climbnote.Data.RoadDataAccess;
import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-05-05.
 */
public class TOP10ragment extends ListFragment  {
    private long areaID = -1;
    private long sectorID = -1;
    private List<Road> data;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.area_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity().getApplicationContext();
        setHasOptionsMenu(true);
        loadData();
    }

    @Override
    public void onResume() {
        loadData();
        super.onResume();
    }

    private void loadData() {
        DataAccess da = RoadDataAccess.create(context);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR)-1;
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);
        c.set(year,m,d);
        int date = ConstDB.dateToString(c);
        data = da.getNames(date);
        if (!data.isEmpty()) {
            ArrayAdapter<Road> adapter = new ArrayAdapter<>(context, R.layout.area_item, data);
            setListAdapter(adapter);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem setting = menu.findItem(R.id.action_settings);
        MenuItem addNew = menu.findItem(R.id.action_add_new);
        setting.setVisible(false);
        addNew.setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_new) {
            Intent empDetailsIntent = new Intent(context, AddRoadActivity.class);
            startActivityForResult(empDetailsIntent, 303);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
