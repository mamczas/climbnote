package mamczas.app.climbnote;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

import java.util.List;

import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.Data.Sector;
import mamczas.app.climbnote.Data.SectorDataAccess;
import mamczas.app.climbnote.constant.ConstDB;


public class SectorListFragment extends ListFragment implements OnItemClickListener, AdapterView.OnItemLongClickListener {
    private List<Sector> data;
    private Context context;
    private Menu menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.area_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity().getApplicationContext();
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
        setHasOptionsMenu(true);
        loadData();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Intent empDetailsIntent = new Intent(context, AddSectorActivity.class);
        Sector sector = data.get(position);
        empDetailsIntent.putExtra(ConstDB.ID, sector.getId());
        startActivityForResult(empDetailsIntent, 202);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new AreaListFragment();
        Sector sector = data.get(position);
        Bundle data = new Bundle();
        data.putLong(ConstDB.SECTOR_ID, sector.getId());

        if(fragment !=null){
            fragment.setArguments(data);
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame,fragment).addToBackStack(null).commit();
        }
    }

    private void loadData() {
        DataAccess da = SectorDataAccess.create(context);
        data = da.getAll();
        ArrayAdapter<Sector> adapter = new ArrayAdapter<>(context, R.layout.area_item, data);
        setListAdapter(adapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem setting = menu.findItem(R.id.action_settings);
        MenuItem addNew = menu.findItem(R.id.action_add_new);
        setting.setVisible(false);
        addNew.setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_add_new:
                Intent empDetailsIntent = new Intent(context, AddSectorActivity.class);
                startActivityForResult(empDetailsIntent, 101);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
           loadData();
        }
    }
}
