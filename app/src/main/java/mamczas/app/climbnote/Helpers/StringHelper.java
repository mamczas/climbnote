package mamczas.app.climbnote.Helpers;

/**
 * Created by michal.mamcarz on 2016-06-03.
 */
public class StringHelper {
    public static String capitalizeWord(String s) {
        if (s.length() == 0) return s;
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static String Capitalize(String args) {
            StringBuilder stringBuilder = new StringBuilder();
            String[] words = args.split("\\s");
            for (String s : words) {
                stringBuilder.append(capitalizeWord(s));
                stringBuilder.append(' ');
            }
            return stringBuilder.toString().trim();
        }
    }

