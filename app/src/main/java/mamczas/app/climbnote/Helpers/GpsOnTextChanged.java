package mamczas.app.climbnote.Helpers;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mamczas.app.climbnote.R;
import mamczas.app.climbnote.constant.geo;

/**
 * Created by michal.mamcarz on 2016-06-02.
 */
public class GpsOnTextChanged  implements TextWatcher {
    private Context mContext;
    private EditText editText;
    private Pattern p = null;
    private FloatingActionButton button;
    private geo type;


    public GpsOnTextChanged(Context context, EditText editText, FloatingActionButton button , geo type) {
        super();
        this.mContext = context;
        this.editText = editText;
        if(type == geo.LATITUDE){
            p = Pattern.compile("^(\\d{1,2})\\s(\\d{1,2})\\s(\\d{1,2})$");
        }else{
            p = Pattern.compile("^(\\d{1,3})\\s(\\d{1,2})\\s(\\d{1,2})$");
        }
        this.type = type;
        this.button = button;
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Matcher m = p.matcher(s);
        button.setEnabled(true);
        button.setBackgroundColor(Color.GREEN);
        boolean effect = true;
        if(m.matches()){
            switch (type){
                case LATITUDE:
                    effect = checkLongtitude(m);
                    break;
                case LONGITUDE:
                    effect = checkLatitude(m);
                    break;
            }
        }
        if(effect){
            editText.setError(mContext.getString(R.string.notvalid),mContext.getDrawable(android.R.drawable.stat_notify_error));
            button.setEnabled(false);
            button.setBackgroundColor(Color.DKGRAY);
        }
    }

    private boolean checkLongtitude(Matcher m){
        int one = Integer.parseInt(m.group(1));
        if(one > 90){return true;}
        one = Integer.parseInt(m.group(2));
        if(one > 60){return true;}
        one = Integer.parseInt(m.group(3));
        if(one > 60){return true;}

        return false;
    }
    private boolean checkLatitude(Matcher m){
        int one = Integer.parseInt(m.group(1));
        if(one > 180){return true;}
        one = Integer.parseInt(m.group(2));
        if(one > 60){return true;}
        one = Integer.parseInt(m.group(3));
        if(one > 60){return true;}

        return false;
    }
}