package mamczas.app.climbnote.constant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by michal.mamcarz on 2016-05-09.
 */
public class ConstDB {
    public static final int DB_VERSION = 1;
    public static final String ID = "_id";
    public static final String DB_NAME = "CLIMB";
    public static final String SECTOR = "SECTORS";
    public static final String SECTOR_ID = "ID_SECTOR";
    public static final String AREA = "AREA";
    public static final String AREA_ID = "ID_AREA";
    public static final String GPS_POSITION = "gpsPosition";
    public static final String NAME = "NAME";
    public static final String ROAD = "ROAD";
    public static final String GRADE = "GRADE";
    public static final String _ID = ID + "=";
    public static final String TIME = "CALENDAR";
    public static final String STYLE = "STYLE";
    public static final String EDIT = "EDIT";
    public static List<Grade> grades;
    public static List<Style> styles;
    private static Map<Integer, String> gradeLevel;
    private static Map<Integer, String> style;

    static {
        gradeLevel = new HashMap<>();
        gradeLevel.put(0, "III");
        gradeLevel.put(1, "IV+");
        gradeLevel.put(2, "V");
        gradeLevel.put(3, "V+");
        gradeLevel.put(4, "VI-");
        gradeLevel.put(5, "VI");
        gradeLevel.put(6, "VI+");
        gradeLevel.put(7, "VI.1");
        gradeLevel.put(8, "VI.1+");
        gradeLevel.put(9, "VI.2");
        gradeLevel.put(10, "VI.2+");
        gradeLevel.put(11, "VI.3");
        gradeLevel.put(12, "VI.3+");
        gradeLevel.put(13, "VI.4");
        gradeLevel.put(14, "VI.4+");
        gradeLevel.put(15, "VI.5");
        gradeLevel.put(16, "VI.5+");
        gradeLevel.put(17, "VI.6");
        gradeLevel.put(18, "VI.6+");
        gradeLevel.put(19, "VI.7");
        gradeLevel.put(20, "VI.7+");
        gradeLevel.put(21, "VI.8");
    }

    static {
        style = new HashMap<>();
        style.put(0, "OS");
        style.put(1, "FLASH");
        style.put(2, "RP");
        style.put(3, "TRAD");
    }

    static {
        grades = new ArrayList<>();
        for (int i = 0; i < gradeLevel.size(); i++) {
            grades.add(new Grade(i, gradeLevel.get(i)));
        }
        styles = new ArrayList<>();
        for (int i = 0; i < style.size(); i++) {
            styles.add(new Style(i, style.get(i)));
        }
    }

    public static Calendar stringToDate(int date) {
        Calendar c = Calendar.getInstance();
        String calendar = Integer.toString(date);
        c.set(Integer.parseInt(calendar.substring(0, 4)), Integer.parseInt(calendar.substring(5, 6)), Integer.parseInt(calendar.substring(7, 8)));
        return c;
    }


    public static int dateToString(Calendar date) {
        String year = Integer.toString(date.get(Calendar.YEAR));
        String month = Integer.toString(date.get(Calendar.MONTH));
        if (month.length() == 1) {
            month = "0" + month;
        }
        String day = Integer.toString(date.get(Calendar.DAY_OF_MONTH));
        if (day.length() == 1) {
            day = "0" + day;
        }
        return Integer.parseInt(year + month + day);
    }
}
