package mamczas.app.climbnote.constant;

/**
 * Created by michal.mamcarz on 2016-09-18.
 */
public class Grade {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }

    public Grade(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    private int id;
    private String desc;

}
