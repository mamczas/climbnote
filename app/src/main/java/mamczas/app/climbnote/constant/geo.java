package mamczas.app.climbnote.constant;

/**
 * Created by michal.mamcarz on 2016-06-02.
 */
public enum geo {
    LATITUDE,
    LONGITUDE
}
