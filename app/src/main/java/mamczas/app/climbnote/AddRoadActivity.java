package mamczas.app.climbnote;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mamczas.app.climbnote.Data.Area;
import mamczas.app.climbnote.Data.AreaDataAccess;
import mamczas.app.climbnote.Data.Road;
import mamczas.app.climbnote.Data.RoadDataAccess;
import mamczas.app.climbnote.Data.Sector;
import mamczas.app.climbnote.Data.SectorDataAccess;
import mamczas.app.climbnote.Fragments.DatePickerFragment;
import mamczas.app.climbnote.Helpers.StringHelper;
import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.Interfaces.OnCompleteListener;
import mamczas.app.climbnote.constant.ConstDB;
import mamczas.app.climbnote.constant.Grade;
import mamczas.app.climbnote.constant.Style;

public class AddRoadActivity extends AppCompatActivity implements OnCompleteListener {
    private long sectorID = -1;
    private long areaID = -1;
    private int styleID = 2;
    private int gradeID = 7;
    private DataAccess dataAccess;
    private List<Sector> dataSpinnerSector;
    private List<Area> dataSpinnerArea;
    private long roadID;
    private DataAccess da;
    private Road r;
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    deleteRowFromDB();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };
    private Calendar date = Calendar.getInstance();
    private Spinner sectorSpinner;
    private Spinner areaSpinner;
    private Spinner styleSpinner;
    private Spinner gradeSpinner;
    private EditText roadName;
    private FloatingActionButton save;
    private FloatingActionButton deleteButton;
    private AlertDialog.Builder confirmDialog = null;

    private void setSpinnerSector() {
        dataAccess = SectorDataAccess.create(this);
        dataSpinnerSector = dataAccess.getNames(-1);
        ArrayAdapter<Sector> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dataSpinnerSector);
        sectorSpinner.setAdapter(adapter);
        if (sectorID != -1) {
            for (int i = 0; i < dataSpinnerSector.size(); i++) {
                if (sectorID == ((Sector) sectorSpinner.getItemAtPosition(i)).getId()) {
                    sectorSpinner.setSelection(i);
                    break;
                }
            }
        }
    }

    private void setSpinnerArea() {
        dataAccess = AreaDataAccess.create(this);
        dataSpinnerArea = dataAccess.getNames(sectorID);
        if (dataSpinnerArea.isEmpty()) {
            List<String> moq = new ArrayList<>();
            moq.add(getString(R.string.select_sector));
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, moq);
            areaSpinner.setAdapter(adapter);
        } else {
            ArrayAdapter<Area> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dataSpinnerArea);
            areaSpinner.setAdapter(adapter);
            if (areaID != -1) {
                for (int i = 0; i < dataSpinnerArea.size(); i++) {
                    if (areaID == ((Area) areaSpinner.getItemAtPosition(i)).getId()) {
                        areaSpinner.setSelection(i);
                        break;
                    }
                }
            }
        }
    }

    private void setSpinnerStyle() {
        ArrayAdapter<Style> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, ConstDB.styles);
        styleSpinner.setAdapter(adapter);
        styleSpinner.setSelection(styleID);
    }

    private void setSpinnerGrade() {
        ArrayAdapter<Grade> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, ConstDB.grades);
        gradeSpinner.setAdapter(adapter);
        gradeSpinner.setSelection(gradeID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_road);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sectorSpinner = (Spinner) findViewById(R.id.spinnerSector);
        areaSpinner = (Spinner) findViewById(R.id.spinnerArea);
        styleSpinner = (Spinner) findViewById(R.id.spinnerStyle);
        gradeSpinner = (Spinner) findViewById(R.id.spinnerGrade);
        roadName = (EditText) findViewById(R.id.edit_name);
        save = (FloatingActionButton) findViewById(R.id.fab);
        if (savedInstanceState == null) {
            Bundle extra = getIntent().getExtras();
            if (extra != null) {
                if (extra.getBoolean(ConstDB.EDIT, false)) {
                    roadID = extra.getLong(ConstDB.ID);
                    da = RoadDataAccess.create(this);
                    r = (Road) da.getById(roadID);
                    sectorID = r.getSectorID();
                    areaID = r.getAreaID();
                    styleID = r.getStyle();
                    gradeID = r.getGrade();
                    date = r.getDate();
                    roadName.setText(r.getName());
                    setDeleteButton();
                } else {
                    sectorID = extra.getLong(ConstDB.SECTOR_ID);
                    areaID = extra.getLong(ConstDB.AREA_ID);
                }
            }
        } else {
            sectorID = savedInstanceState.getLong(ConstDB.SECTOR_ID);
            areaID = savedInstanceState.getLong(ConstDB.AREA_ID);
            styleID = savedInstanceState.getInt(ConstDB.STYLE);
            areaID = savedInstanceState.getInt(ConstDB.GRADE);
            date = ConstDB.stringToDate(savedInstanceState.getInt(ConstDB.TIME));
        }
        setSpinnerSector();
        setSpinnerArea();
        setSpinnerStyle();
        setSpinnerGrade();
    }

    @Override
    protected void onStart() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabClicked();
            }
        });
        styleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                styleID = ConstDB.styles.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        gradeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gradeID = ConstDB.grades.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (areaSpinner.getItemAtPosition(position) instanceof Area ) {
                    areaID = ((Area) areaSpinner.getItemAtPosition(position)).getId();
                }
                else{
                    areaID = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sectorID = ((Sector) sectorSpinner.getItemAtPosition(position)).getId();
                setSpinnerArea();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        super.onStart();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onComplete(Calendar date) {
        this.date = date;
    }

    @Override
    public Calendar onStartDialog() {
        return this.date;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(ConstDB.SECTOR_ID, sectorID);
        outState.putLong(ConstDB.AREA_ID, areaID);
        outState.putLong(ConstDB.STYLE, styleID);
        outState.putLong(ConstDB.GRADE, gradeID);
        outState.putInt(ConstDB.TIME, ConstDB.dateToString(date));
        super.onSaveInstanceState(outState);
    }

    protected void fabClicked() {
        if (areaID != -1) {
            da = RoadDataAccess.create(this);
            String s = StringHelper.Capitalize(roadName.getText().toString());
            if (s.matches("")) {
                Toast.makeText(this, R.string.YOU_DID_NOT_ENTER_ROAD_NAME, Toast.LENGTH_SHORT).show();
                return;
            }
            r = new Road();
            r.setName(s);
            r.setGrade(gradeID);
            r.setStyle(styleID);
            r.setDate(date);
            r.setArea(new Area(areaID));
            r.setDate(date);
            da.put(r);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(ConstDB.AREA_ID, areaID);
            returnIntent.putExtra(ConstDB.SECTOR_ID, sectorID);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    private void setDeleteButton() {
        deleteButton = (FloatingActionButton) findViewById(R.id.delete_sector);
        deleteButton.setVisibility(View.VISIBLE);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteClicked();
            }
        });
    }

    protected void deleteClicked() {
        confirmDialog = new AlertDialog.Builder(this);
        confirmDialog.setTitle(getString(R.string.confirm));
        confirmDialog.setMessage(getString(R.string.Question_delete_item));
        confirmDialog.setPositiveButton(getString(R.string.yes), dialogClickListener);
        confirmDialog.setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }

    private void deleteRowFromDB() {
        da.delete(r.getId());
    }
}
