package mamczas.app.climbnote.Fragments.Models;

/**
 * Created by michal.mamcarz on 2016-09-20.
 */
public class WeatherStatus {
    public String getUrl() {
        return url;
    }

    public String getTemp() {
        return temp;
    }

    public String getPrecip() {
        return precip;
    }



    public WeatherStatus(String url, String temp,String precip) {
        this.url = url;
        this.temp = temp;
        this.precip = precip;
    }

    String url;
    String temp;
    String precip;

}
