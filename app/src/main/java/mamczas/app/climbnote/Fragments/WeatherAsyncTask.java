package mamczas.app.climbnote.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.regex.Pattern;

import mamczas.app.climbnote.AddSectorActivity;
import mamczas.app.climbnote.Fragments.Models.WeatherStatus;
import mamczas.app.climbnote.R;

/**
 * Created by michal.mamcarz on 2016-09-19.
 */
public class WeatherAsyncTask extends AsyncTask<String, Void, WeatherStatus> {

    private final Context context;
    private String getLocation = "http://api.wunderground.com/api/57445ca2b84e600c/geolookup/q/"; //50.15,19.13.json
    private String getWeather = "http://api.wunderground.com/api/57445ca2b84e600c/conditions/q/"; //PL/Jaworzno.json

    public WeatherAsyncTask(Context ctx) {
        context = ctx;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected WeatherStatus doInBackground(String... params) {
        Log.d("HttpRequestTask", "HTTP Request started ...");
        try {
            String lat = params[0];
            String lon = params[1];
            JSONObject response;
            URL url = new URL(getLocation + lat + "," + lon + ".json");
            response = getJsonObject(url);
            JSONArray listOfStations = readStations(response);
            if (listOfStations != null) {
                String city;
                for (int i = 0; i < listOfStations.length(); i++) {
                    city = GetStationLocation(listOfStations.getJSONObject(i));
                    if (city != null) {
                        url = new URL(getWeather + city);
                        response = getJsonObject(url);
                        JSONObject observation = GetObservation(response);
                        if (observation != null) {
                            String temp = readTEMP(observation);
                            String link = readURL(observation);
                            String precip = readPrecip(observation);
                            if (temp != null && link != null && precip != null) {
                                return new WeatherStatus(link, temp, precip);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("HttpRequestTask", "Error loading weather.", ex);
            return null;
        }
        return null;
    }

    private JSONObject getJsonObject(URL url) throws IOException {
        JSONObject response;
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            response = readStream(in);
        } finally {
            urlConnection.disconnect();
        }
        return response;
    }

    private JSONObject GetObservation(JSONObject response) {
        JSONObject current_observation = null;
        try {
            if (response != null) {
                current_observation = response.getJSONObject("current_observation");
            }
            return current_observation;
        } catch (JSONException e) {
            return null;
        }
    }

    private String readURL(JSONObject response) {
        String url = null;
        try {
            if (response != null) {
                url = response.getString("forecast_url");
            }
            return url;
        } catch (JSONException e) {
            return null;
        }
    }

    private String readTEMP(JSONObject response) {
        String temp = null;
        try {
            if (response != null) {
                temp = response.getString("temp_c");
            }
            return "Temp." + temp + "°C";
        } catch (JSONException e) {
            return null;
        }
    }

    private String readPrecip(JSONObject response) {
        String precip = null;
        try {
            if (response != null) {
                precip = response.getString("precip_today_metric");
            }
            return "Precip " + precip + " mm";
        } catch (JSONException e) {
            return null;
        }
    }

    private JSONArray readStations(JSONObject response) {
        try {
            if (response != null) {
                JSONArray stations = response.getJSONObject("location").getJSONObject("nearby_weather_stations")
                        .getJSONObject("pws").getJSONArray("station");
                return stations;
            }
        } catch (JSONException e) {
            return null;
        }
        return null;
    }

    private String GetStationLocation(JSONObject jsonObject) {
        if (jsonObject != null) {
            String countryCode = null;
            String city;
            try {
                countryCode = jsonObject.getString("country");
                city = jsonObject.getString("city");
                if (city != null && countryCode != null) {
                    return "/" + countryCode + "/" + deAccent(city) + ".json";
                }
            } catch (JSONException e) {

            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(final WeatherStatus result) {
        Log.d("WeatherAsyncTask", "HTTP Request finished");
        if (result != null) {
            Activity act = (Activity) context;
            Button button = (Button) act.findViewById(R.id.weather);
            TextView tempView = (TextView) act.findViewById(R.id.temp);
            TextView precipView = (TextView) act.findViewById(R.id.precip);
            button.setVisibility(View.VISIBLE);
            ((AddSectorActivity) act).setWeatherStatus(result);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(result.getUrl())));
                }
            });
            tempView.setText(result.getTemp());
            precipView.setText(result.getPrecip());
        } else {
            Toast.makeText(context, "Error loading Weather.", Toast.LENGTH_LONG).show();
        }
    }

    private JSONObject readStream(InputStream inputStream) {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject jsonObject;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            String line;
            while ((line = r.readLine()) != null) {
                stringBuilder.append(line);
            }
            jsonObject = new JSONObject(stringBuilder.toString());
            return jsonObject;
        } catch (Exception ex) {
            return null;
        }

    }

    private String deAccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }
}
