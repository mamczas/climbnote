package mamczas.app.climbnote.Fragments;

/**
 * Created by michal.mamcarz on 2016-07-05.
 */

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import java.util.ArrayList;

import mamczas.app.climbnote.Interfaces.TaskStatusCallback;

public class HeadlessTimerFragment extends Fragment {

    public static final String TAG_HEADLESS_FRAGMENT = "headless_fragment";


    TaskStatusCallback mStatusCallback;
    BackgroundTask mBackgroundTask;
    boolean isTaskExecuting = false;

    /**
     * Called when a fragment is first attached to its activity.
     * onCreate(Bundle) will be called after this.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mStatusCallback = (TaskStatusCallback) activity;
    }

    /**
     * Called to do initial creation of a fragment.
     * This is called after onAttach(Activity) and before onCreateView(LayoutInflater, ViewGroup, Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    /**
     * Called when the fragment is no longer attached to its activity. This is called after onDestroy().
     */
    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        mStatusCallback = null;
    }

    public void startBackgroundTask(ArrayList<Integer> params) {
        if (!isTaskExecuting) {
            mBackgroundTask = new BackgroundTask(params);
            mBackgroundTask.execute();
            isTaskExecuting = true;
        }
    }

    public void cancelBackgroundTask() {
        if (isTaskExecuting) {
            mBackgroundTask.cancel(true);
            isTaskExecuting = false;
        }
    }

    public void updateExecutingStatus(boolean isExecuting) {
        this.isTaskExecuting = isExecuting;
    }

    private class BackgroundTask extends AsyncTask<Void, Integer, Void> {

        int rest;
        int hang;
        int rep;

        private BackgroundTask(ArrayList<Integer> params) {
            rest = params.get(2);
            hang = params.get(1);
            rep = params.get(0);
        }

        @Override
        protected void onPreExecute() {
            if (mStatusCallback != null)
                mStatusCallback.onPreExecute();
        }

        private void sleep() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            int tmpRest = rest;
            int tmpHang = hang;
            int tmpRep = rep;

            while (!isCancelled()) {
                sleep();
                if (rest > 0) {
                    rest -= 1;
                    publishProgress(rep,hang,rest);
                    continue;
                }
                if (hang > 0) {
                    hang -= 1;
                    publishProgress(rep,hang,rest);
                    continue;

                }
                if (hang == 0) {
                    rep -= 1;
                    publishProgress(rep,hang,rest);
                    rest = tmpRest;
                    hang = tmpHang;
                }
                if (rep <= 0) {
                    return null;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mStatusCallback != null)
                mStatusCallback.onPostExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (mStatusCallback != null)
                mStatusCallback.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Void result) {
            if (mStatusCallback != null)
                mStatusCallback.onCancelled();
        }

    }

}
