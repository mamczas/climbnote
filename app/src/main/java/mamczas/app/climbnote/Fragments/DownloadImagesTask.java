package mamczas.app.climbnote.Fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mamczas.app.climbnote.R;

/**
 * Created by michal.mamcarz on 2016-09-20.
 */
public class DownloadImagesTask extends AsyncTask<String, Void, Bitmap> {

    private final Activity activity;

    public DownloadImagesTask(Activity context) {
        this.activity = context;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String link = params[0];
        try{
            URL url = new URL(link);
            InputStream in = call(url);
            BufferedInputStream bis = new BufferedInputStream(in);
            Bitmap bm = BitmapFactory.decodeStream(bis);
            bis.close();
           return bm;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap != null){

            ImageView image = (ImageView) activity.findViewById(R.id.weather);
            image.setImageBitmap(bitmap);
        }
        super.onPostExecute(bitmap);
    }


    private InputStream call(URL url) throws IOException {
        InputStream in;
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            in = new BufferedInputStream(urlConnection.getInputStream());
        } finally {
            urlConnection.disconnect();
        }
        return in;
    }
}
