package mamczas.app.climbnote;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mamczas.app.climbnote.Data.Area;
import mamczas.app.climbnote.Data.AreaDataAccess;
import mamczas.app.climbnote.Data.GPS;
import mamczas.app.climbnote.Data.Sector;
import mamczas.app.climbnote.Data.SectorDataAccess;
import mamczas.app.climbnote.Helpers.InputFilterMinMaxDouble;
import mamczas.app.climbnote.Helpers.InputFilterMinMaxInt;
import mamczas.app.climbnote.Helpers.MyLocationListener;
import mamczas.app.climbnote.Helpers.StringHelper;
import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;


public class AddAreaActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    private static final String TAG = "LocationActivityArea";
    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime;
    private Spinner drop_logntitude;
    private Spinner drop_latitude;
    private EditText areaName;
    private EditText st_longtitude;
    private EditText min_longtitude;
    private EditText sec_longtitude;
    private EditText st_latitude;
    private EditText min_latitude;
    private EditText sec_latitude;
    private TextView latitude;
    private TextView logntitude;
    private ViewFlipper vf;
    private Switch switch_GPS;
    private Spinner sectorSpinner;
    private long areaID;
    private long sectorID;
    private List<Sector> dataSector;
    private Area area = null;
    private FloatingActionButton deleteButton;
    private DataAccess da;
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    deleteRowFromDB();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };
    private AlertDialog.Builder confirmDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_area);
        Toolbar toolbar = (Toolbar) findViewById(R.id.saveSector);
        Button map = (Button) findViewById(R.id.mapButton);
//        map.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(this, MapsActivity.class)
//                startActivity( new Intent(this, MapsActivity.class));
//            }
//        });
        setSupportActionBar(toolbar);

        findElementsInView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        setSpiners();

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);

        // Kick off the process of building a GoogleApiClient and requesting the LocationServices
        // API.
        buildGoogleApiClient();
        if (switch_GPS != null) {
            switch_GPS.setOnCheckedChangeListener(this);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabClicked();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setSpiners() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.logntitude, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        drop_logntitude.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this,
                R.array.latitude, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        drop_latitude.setAdapter(adapter);
    }

    private void setSectorSpinner() {
        da = SectorDataAccess.create(this);
        dataSector = da.getAll();
        ArrayAdapter<Sector> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dataSector);
        sectorSpinner.setAdapter(adapter);
        if (sectorID != -1) {
            for (int i = 0; i < dataSector.size(); i++) {
                if (sectorID == ((Sector) sectorSpinner.getItemAtPosition(i)).getId()) {
                    sectorSpinner.setSelection(i);
                    break;
                }
            }
        }
    }

    protected void fabClicked() {
        String s = StringHelper.Capitalize(areaName.getText().toString());
        if (s.matches("")) {
            Toast.makeText(this, R.string.YOU_DID_NOT_ENTER_AREA, Toast.LENGTH_SHORT).show();
            return;
        }
        da = AreaDataAccess.create(getApplicationContext());
        GPS gps;
        if (switch_GPS.isChecked()) {
            gps = new GPS(mCurrentLocation.getLongitude(), mCurrentLocation.getLatitude());
        } else {
            gps = new GPS(getLongGPS(), getLatGPS());
        }
        Sector sector = new Sector();
        //spinner enumerable form 0 but sqLite from 1
        long idSector = sectorSpinner.getSelectedItemId() + 1;
        sector.setId(idSector);
        area = new Area(s, gps, sector);
        long resultAreaID = da.put(area);
        Intent returnIntent = new Intent();
        returnIntent.putExtra(ConstDB.AREA_ID, resultAreaID);
        returnIntent.putExtra(ConstDB.SECTOR_ID, idSector);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void setDeleteButton() {
        //set navigation
        Button nav = (Button) findViewById(R.id.mapButton);
        nav.setVisibility(View.VISIBLE);
        nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse(String.format(Locale.US,"google.navigation:q=%.6f,%.6f",area.getPosition().getLatitude(), area.getPosition().getLongitude()));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        //set delete
        deleteButton = (FloatingActionButton) findViewById(R.id.delete_area);
        deleteButton.setVisibility(View.VISIBLE);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteClicked();
            }
        });
    }

    protected void deleteClicked() {
        confirmDialog = new AlertDialog.Builder(this);
        confirmDialog.setTitle(getString(R.string.confirm));
        confirmDialog.setMessage(getString(R.string.Question_delete_item));
        confirmDialog.setPositiveButton(getString(R.string.yes), dialogClickListener);
        confirmDialog.setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }

    private void deleteRowFromDB() {
        da.delete(area.getId());
    }

    private Double getLongGPS() {
        try {
            double stLong = getNumber(st_longtitude.getText().toString(), 0.0);
            double minLong = getNumber(min_longtitude.getText().toString(), 0.0) / 60;
            double secLong = getNumber(sec_longtitude.getText().toString(), 0.0) / 3600;
            stLong += minLong + secLong;
            if(stLong > 180){
                stLong = 180;
            }
            switch (drop_logntitude.getSelectedItemPosition()) {
                case 0:
                    return stLong;
                case 1:
                    return  -1 *stLong;
                default:
                    return stLong;

            }
        } catch (Exception e) {
            return 0.0;
        }
    }
    private Double getLatGPS() {
        try {
            double stlatitude = getNumber(st_latitude.getText().toString(), 0.0);
            double minlatitude = getNumber(min_latitude.getText().toString(), 0.0) / 60;
            double seclatitude = getNumber(sec_latitude.getText().toString(), 0.0) / 3600;
            stlatitude += minlatitude + seclatitude;
            if(stlatitude > 90){
                stlatitude = 90.0;
            }
            switch (drop_latitude.getSelectedItemPosition()) {
                case 0:
                    return stlatitude;
                case 1:
                    return -1 * stlatitude;
                default:
                    return stlatitude;

            }
        } catch (Exception e) {
            return 0.0;
        }
    }
    private double getNumber(String edtValue, double defaultValue) {
        double value = defaultValue;

        if (edtValue != null) {
            try {
                value = Double.parseDouble(edtValue);
            } catch (NumberFormatException e) {
                value = defaultValue;
            }
        }
        return value;
    }

    private void setEditors() {
        areaName.setText(area.getName());
        drop_logntitude.setSelection(area.getPosition().getlongitudeNEWS());
        st_longtitude.setText(String.valueOf(area.getPosition().getLongitudeST()));
        min_longtitude.setText(String.valueOf(area.getPosition().getLongitudeMin()));
        sec_longtitude.setText(String.format(Locale.ROOT,"%.2f", area.getPosition().getLongitudeSEC()));

        drop_latitude.setSelection(area.getPosition().getlatitudeNEWS());
        st_latitude.setText(String.valueOf(area.getPosition().getLatitudeST()));
        min_latitude.setText(String.valueOf(area.getPosition().getLatitudeMin()));
        sec_latitude.setText(String.format(Locale.ROOT,"%.2f", area.getPosition().getLatitudeSEC()));
    }

    public int GPSEnabled() {
        try {
            int off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            return off;
        } catch (Settings.SettingNotFoundException e) {
            return 0;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (GPSEnabled() == 0) {
                MyLocationListener.showSettingsAlert(this);
            } else {
                vf.setDisplayedChild(1);
                startLocationUpdates();
            }
        } else {
            stopLocationUpdates();
            vf.setDisplayedChild(0);
        }
    }

    private void setAutoGPS() {
        if (mCurrentLocation != null) {
            latitude.setText(Location.convert(mCurrentLocation.getLatitude(), Location.FORMAT_SECONDS));
            logntitude.setText(Location.convert(mCurrentLocation.getLongitude(), Location.FORMAT_SECONDS));
        }
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        Log.i(TAG, "Updating values from bundle");
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
                switch_GPS.setChecked(mRequestingLocationUpdates);
                if (mRequestingLocationUpdates == true) {
                    vf.setDisplayedChild(1);
                }
            }
            if(savedInstanceState.keySet().contains(ConstDB.AREA_ID)){
                areaID = savedInstanceState.getLong(ConstDB.AREA_ID);
                loadArea();
                setDeleteButton();
            }
            if(savedInstanceState.keySet().contains(ConstDB.SECTOR_ID)){
                sectorID = savedInstanceState.getLong(ConstDB.SECTOR_ID);
                setSectorSpinner();
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
            }
            setAutoGPS();
        }else{
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                areaID = extras.getLong(ConstDB.ID, -1);
                sectorID = extras.getLong(ConstDB.SECTOR_ID, -1);
                setSectorSpinner();
                if (areaID != -1) {
                    setDeleteButton();
                    loadArea();
                    setEditors();
                }
            }
        }
    }

    private void loadArea() {
        da = AreaDataAccess.create(this);
        area = (Area) da.getById(areaID);
        sectorID = area.getSector().getId();
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        try {
            if (mGoogleApiClient.isConnected()) {
                mRequestingLocationUpdates = true;
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);
            }
        } catch (SecurityException e) {}
    }


    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        if (mGoogleApiClient.isConnected()) {
            mRequestingLocationUpdates = false;
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.

        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        try {
            Log.i(TAG, "Connected to GoogleApiClient");

            if (mCurrentLocation == null) {
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                setAutoGPS();
            }

            if (mRequestingLocationUpdates) {
                startLocationUpdates();
            }
        } catch (SecurityException e) {

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        setAutoGPS();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        savedInstanceState.putLong(ConstDB.AREA_ID, areaID);
        savedInstanceState.putLong(ConstDB.SECTOR_ID, sectorID);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void findElementsInView() {
        switch_GPS = (Switch) findViewById(R.id.switch_GPS);
        vf = (ViewFlipper) findViewById(R.id.vf);
        latitude = (TextView) findViewById(R.id.txt_latidude);
        logntitude = (TextView) findViewById(R.id.txt_longtitude);
        drop_logntitude = (Spinner) findViewById(R.id.drop_logntitude);
        drop_latitude = (Spinner) findViewById(R.id.drop_latitude);
        st_longtitude = (EditText) findViewById(R.id.st_long);
        st_longtitude.setFilters(new InputFilter[]{new InputFilterMinMaxInt(0, 180)});
        min_longtitude = (EditText) findViewById(R.id.min_long);
        min_longtitude.setFilters(new InputFilter[]{new InputFilterMinMaxInt(0, 59)});
        sec_longtitude = (EditText) findViewById(R.id.sec_long);
        sec_longtitude.setFilters(new InputFilter[]{new InputFilterMinMaxDouble(0, 59.99)});
        st_latitude = (EditText) findViewById(R.id.st_Latitude);
        st_latitude.setFilters(new InputFilter[]{new InputFilterMinMaxInt(0, 90)});
        min_latitude = (EditText) findViewById(R.id.min_Latitude);
        min_latitude.setFilters(new InputFilter[]{new InputFilterMinMaxInt(0, 59)});
        sec_latitude = (EditText) findViewById(R.id.sec_Latitude);
        sec_latitude.setFilters(new InputFilter[]{new InputFilterMinMaxDouble(0, 59.99)});
        areaName = (EditText) findViewById(R.id.edit_nameArea);
        sectorSpinner = (Spinner) findViewById(R.id.spinner_Sector);
    }

}
