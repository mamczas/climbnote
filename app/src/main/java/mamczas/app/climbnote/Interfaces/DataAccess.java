package mamczas.app.climbnote.Interfaces;

/**
 * Created by michal.mamcarz on 2016-05-17.
 */

import android.database.Cursor;

import java.util.HashMap;
import java.util.List;

public interface DataAccess<T> {

    List<T> getAll();

    long insert(T e);

    void update(T e);

    void delete(T e);

    void delete(long id);

    T getById(long id);

    Cursor getCursor();

    long isInDB(String name);

    long put(T e);

    List<T> getListByID(long id);

    List<T> getNames(long id);
}