package mamczas.app.climbnote.Interfaces;

import java.util.ArrayList;

/**
 * Created by michal.mamcarz on 2016-07-05.
 */
public interface TaskStatusCallback {
    void onPreExecute();

    void onProgressUpdate(Integer... parameters);

    void onPostExecute();

    void onCancelled();
}