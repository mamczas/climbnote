package mamczas.app.climbnote.Interfaces;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by michal.mamcarz on 2016-09-18.
 */
public interface OnCompleteListener {
    public abstract void onComplete(Calendar date);
    public abstract Calendar onStartDialog();
}