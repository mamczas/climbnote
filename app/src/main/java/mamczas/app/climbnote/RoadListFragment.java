package mamczas.app.climbnote;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

import java.util.List;

import mamczas.app.climbnote.Data.Road;
import mamczas.app.climbnote.Data.RoadDataAccess;
import mamczas.app.climbnote.Interfaces.DataAccess;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-05-05.
 */
public class RoadListFragment extends ListFragment implements OnItemClickListener, AdapterView.OnItemLongClickListener {
    private long areaID = -1;
    private long sectorID = -1;
    private List<Road> data;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.area_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity().getApplicationContext();
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
        setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            areaID = savedInstanceState.getLong(ConstDB.AREA_ID, -1);
            sectorID = savedInstanceState.getLong(ConstDB.SECTOR_ID, -1);
        }
        Bundle extras = getArguments();
        if (extras != null) {
            areaID = extras.getLong(ConstDB.AREA_ID);
            sectorID = extras.getLong(ConstDB.SECTOR_ID);
        }
        loadData(areaID);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    private void loadData(long id) {
        DataAccess da = RoadDataAccess.create(context);
        if (id == -1) {
            data = da.getAll();
        } else {
            data = da.getListByID(id);
        }
        if (!data.isEmpty()) {
            ArrayAdapter<Road> adapter = new ArrayAdapter<>(context, R.layout.area_item, data);
            setListAdapter(adapter);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem setting = menu.findItem(R.id.action_settings);
        MenuItem addNew = menu.findItem(R.id.action_add_new);
        setting.setVisible(false);
        addNew.setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_new) {
            Intent empDetailsIntent = new Intent(context, AddRoadActivity.class);
            empDetailsIntent.putExtra(ConstDB.AREA_ID, areaID);
            empDetailsIntent.putExtra(ConstDB.SECTOR_ID, sectorID);
            startActivityForResult(empDetailsIntent, 303);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(ConstDB.AREA_ID, areaID);
        outState.putLong(ConstDB.SECTOR_ID, sectorID);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bundle extra = data.getExtras();
            if (extra != null) {
                areaID = extra.getLong(ConstDB.AREA_ID, -1);
                sectorID = extra.getLong(ConstDB.SECTOR_ID, -1);
                loadData(areaID);
            } else {
                loadData(-1);
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Intent empDetailsIntent = new Intent(context, AddRoadActivity.class);
        Road road = data.get(position);
        empDetailsIntent.putExtra(ConstDB.ID, road.getId());
        empDetailsIntent.putExtra(ConstDB.EDIT, true);
        startActivityForResult(empDetailsIntent, 302);
        return true;
    }
}
