package mamczas.app.climbnote.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import mamczas.app.climbnote.R;
import mamczas.app.climbnote.constant.ConstDB;

/**
 * Created by michal.mamcarz on 2016-05-10.
 */
public class RoadAdapter extends CursorAdapter {
    private static final int CURSOR_TEXT_COLUMN = 0;

    public RoadAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }

//    @Override
//    public void bindView(View view, Context context, Cursor cursor) {
//        ViewHolder holder = (ViewHolder) view.getTag();
//        holder.text.setText(cursor.getString(CURSOR_TEXT_COLUMN));
//        holder.progress
//                .setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//
//                    @Override
//                    public void onRatingChanged(RatingBar ratingBar,
//                                                float rating, boolean fromUser) {
//                        // basic example on how you may update the
//                        // TextView(you could use a tag etc).
//                        // Keep in mind that if you scroll this row and come
//                        // back the value will reset as you need to save the
//                        // new rating in a more persistent way and update
//                        // the progress
//                        View rowView = (View) ratingBar.getParent();
//                        TextView text = (TextView) rowView
//                                .findViewById(R.id.name);
//                        text.setText(String.valueOf(rating));
//                    }
//                });
//    }
//
//    @Override
//    public View newView(Context context, Cursor cursor, ViewGroup parent) {
//        LayoutInflater mInflater = LayoutInflater.from(context);
//        View rowView = mInflater.inflate(R.layout.row_layout, parent,
//                false);
//        ViewHolder holder = new ViewHolder();
//        holder.text = (TextView) rowView.findViewById(R.id.the_text);
//        holder.progress = (RatingBar) rowView
//                .findViewById(R.id.the_progress);
//        rowView.setTag(holder);
//        return rowView;
//    }
//
//    static class ViewHolder {
//        TextView text;
//        RatingBar progress;
//    }

}